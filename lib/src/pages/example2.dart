
import 'package:flutter/material.dart';

class  Example2 extends StatelessWidget {
  const Example2 ({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ListView2'),
      ),
      body: ListView(
        children: [
          ListTile(
            leading: Icon(Icons.directions_railway,
        size: 25,
          ),
      title: Text(
        '8.00 A.M.',
        style: TextStyle(fontSize: 18),

      ),
      subtitle: Text(
        'Hello world XXXX'
      ),
      trailing: Icon(
        Icons.notifications_none,
        size: 25,
      ),
            onTap: (){
              print('Train');
            },
      ),
          ListTile(
            leading: Icon(Icons.directions_boat,
              size: 25,
            ),
            title: Text(
              '10.00 A.M.',
              style: TextStyle(fontSize: 18),

            ),
            subtitle: Text(
                'Hello world XXXX'
            ),
            trailing: Icon(
              Icons.notifications_off_rounded,
              size: 25,
            ),
            onTap: (){
              print('Boat');
            },
          ),
          ListTile(
            leading: Icon(Icons.directions_ferry,
              size: 25,
            ),
            title: Text(
              '12.00 A.M.',
              style: TextStyle(fontSize: 18),

            ),
            subtitle: Text(
                'Hello world XXXX'
            ),
            trailing: Icon(
              Icons.notifications_none,
              size: 25,
            ),
            onTap: (){
              print('Ferry');
            },
          ),
          ListTile(
            leading: Icon(Icons.directions_car,
              size: 25,
            ),
            title: Text(
              '1.00 A.M.',
              style: TextStyle(fontSize: 18),

            ),
            subtitle: Text(
                'Hello world XXXX'
            ),
            trailing: Icon(
              Icons.notifications_none,
              size: 25,
            ),
            onTap: (){
              print('Car');
            },
          ),
        ],
      ),
    );
  }
}
