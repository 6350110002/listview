
import 'package:flutter/material.dart';

class Example3 extends StatelessWidget {
   Example3({Key? key}) : super(key: key);

  final titles = ['1.Lewandowski ', '2.Messi', '3.Henry', '4.Ter Stegen',
    '5.Puyol', '6.Sergio Busquets', '7.Neymar', '8.Rakitic', '9.Araujo'];
  
   final subtitle = ['NUMBER 9', 'NUMBER 10', 'NUMBER 12', 'NUMBER 1',
     'NUMBER 4', 'NUMBER 5', 'NUMBER 11', 'NUMBER 99', 'NUMBER 3',];

   final image = [
     NetworkImage('https://static.thairath.co.th/media/4DQpjUtzLUwmJZZSEmFsLvSoLOGK72mAeY3IfwZoiYAf.jpg'),
     NetworkImage('https://www.fcbarcelona.com/fcbarcelona/photo/2019/10/29/6b85db1d-6b6b-4da4-9f86-3db7f4278503/mini_2019-10-29_FCBvsVALLADOLID_04.jpg'),
     NetworkImage('https://www.fcbarcelona.com/photo-resources/2020/04/14/713991f3-11a7-4f6a-8213-2242ad18bdd7/HENRY.png?width=600&height=820'),
     NetworkImage('https://assets.goal.com/v3/assets/bltcc7a7ffd2fbf71f5/blt261525959dc2a3bd/60db3ba920a5380ed1a167c6/030328dd353fb3bbdafceeb7723f13690e878cbe.jpg'),
     NetworkImage('https://img.bleacherreport.net/img/images/photos/002/783/824/52d0201bfa6d75082c818a29c1cef0ed_crop_north.jpg?1393948998&w=3072&h=2048'),
     NetworkImage('https://images2.minutemediacdn.com/image/upload/c_fill,w_912,h_516,f_auto,q_auto,g_auto/shape/cover/sport/FC-Barcelona-v-Villarreal-CF---La-Liga-Santander-ab878364c32066519e1b0349310697f6.jpg'),
     NetworkImage('https://i.pinimg.com/originals/99/94/7b/99947bba3fe9fe57f4993b15b58a5a2c.png'),
     NetworkImage('https://e0.365dm.com/20/04/2048x1152/skysports-ivan-rakitic-barcelona_4968194.jpg'),
     NetworkImage('https://www.fcbarcelona.com/fcbarcelona/photo/2021/09/20/93706e50-69f1-4015-9803-5550321538b9/mini_2021-09-20_FCBvsGRANADA_60.jpg'),
   ];
   // final icons = [Icons.directions_bike, Icons.directions_boat,
   //  Icons.directions_bus, Icons.directions_car, Icons.directions_railway,
   //  Icons.directions_run, Icons.directions_subway, Icons.directions_transit,
   //  Icons.directions_walk];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('FC Barcelona'),
      ),
      body: ListView.builder(
        itemCount: titles.length,
        itemBuilder: (context,index){
        return Column(
          children: [
            ListTile(
              leading: CircleAvatar(backgroundImage: image[index],),
              title: Text('${titles[index]}', style: TextStyle(fontSize: 18),),
              subtitle: Text('${subtitle[index]}',style: TextStyle(fontSize: 15),),
              trailing: Icon(Icons.notifications_none,size: 25,),
            ),
            Divider(thickness: 1,)
          ],
        );
    }
      )
    );
  }
}
